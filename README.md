Provision EC2 instance using Terraform and install software and deploy app using Ansible

Create an EC2 instance

Create a Security Group and whitelist the port 22 & 8080

Create a Dynamo table

Fetch the Public IP

Run Ansible Playbook 

	- name: Install Apache on EC2 Instance

    - name: Update apt cache
     
    - name: Update all apt packages
     
    - name: Install Apache
      
![Screenshot 2022-06-20 at 9 55 23 AM](https://user-images.githubusercontent.com/58227542/174525141-39a32cbc-f215-4ebd-93f8-2e836ceb264e.png)






